<?php

/**
 * @file
 * Default theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block. In
 *     that case the class would be 'block-user'.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 *
 * @ingroup themeable
 */

?><?php
if ($block->module=="menu_block" && $is_front) {
	





	?><div class="row"><div class="page-header col-lg-12">
		<h1 class="title" id="page-title">Duke Development</h1>
	</div></div><?php


	?><div class="row"><?php


	$links = $variables['elements']['#content'];
	foreach ($links as $link) {
		if (!isset($link['#title']) || $link['#title']=='' || $link['#title']=='Home') {continue;}
		$title = $link['#title'];
		$href = $link['#href'];
		$description = $link['#localized_options']['attributes']['title'];
		$uri = 'public://';
		$path= file_create_url($uri);
		$image_src = file_exists(drupal_realpath('public://').'/'.$title.'.png')?$path.$title.'.png':$path.'default.png';
		//$image_src = $path.$title.'.png';

		?><div class="front-menu-item col-xs-6 col-sm-4 col-lg-3"><?php
		print '<a class="thumbnail front-menu-box" href="'.$href.'">';
		print '<img src="'.$image_src.'" alt="'.$title.'">';
		print '<h3 class="front-menu-box-title">'.$title.'</h3>';
		print '<p>'.$description.'</p>';

		//print_r($link);
		
		?></a><?php

		if (count($link['#below'])>0) {
			?><div class="front-menu-sublink-frame"><div class="thumbnail front-menu-sublink-box"><?php
			print "<h4>Sub-Resources</h4>";
			print "<ul>";
			foreach($link['#below'] as $sublink) {
				if (!isset($sublink['#title']) || $sublink['#title']=='') {continue;}
				print '<li><a href="'.$sublink['#href'].'">'.$sublink['#title'].'</a></li>';
			};
			print "</ul>";
			?></div></div><?php
		}
		?></div><?php
	}
	
	//variables[elements][#content][][#href]
	//variables[elements][#content][][#title]
	
	
	?></div><?php
	
} elseif ($block->module=="menu_block") { ?> 
	







<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <form class="navbar-form navbar-right in" role="search" id="navbar-search-form">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search" id="search-text-area">
        </div>
        <button type="submit" class="btn btn-default" id="search-submit-button">Submit</button>
      </form>
      <a class="navbar-brand" href="#">Menu</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
	<?php $links = $variables['elements']['#content']; ?>
        <?php foreach($links as $link) { ?>
                <?php
		if (!isset($link['#title']) || $link['#title']=='') {continue;}
		$title = $link['#title'];
		$href = $link['#href'];
		$uri = 'public://';
                $path = file_create_url($uri);
		$image_src = file_exists(drupal_realpath('public://').'/'.$title.'.png')?$path.$title.'.png':$path.'default.png';
                ?>




                <li <?php if (isset($link['attributes']['class']) && strpos(implode($link['attributes']['class']),'active-trail')!==false) {print 'class="active"';} ?>>
                <a href="<?php print $href=='<front>'?'/':'/'.$href; ?>">
                        <?php //print $link['title']; 
                                print '<img class="small_icon" src="'.$image_src.'" alt="'.$title.'">';
                                print $title;
                        ?>
                </a>
                </li>
        <?php } ?>
      </ul>



    </div><!-- /.navbar-collapse -->

  </div><!-- /.container-fluid -->
</nav>

<script>
var searchbuttonprepared = (function() {
        form = document.getElementById("navbar-search-form");
        form.addEventListener('submit', function(evt) {
                evt.preventDefault();
                searchterms = encodeURIComponent(document.getElementById("search-text-area").value);
                window.location.pathname = "/search/node/"+searchterms;
        });
        
        return true;
})();
</script>









	
<?php } else { ?>
	<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

	  <?php print render($title_prefix); ?>
	<?php if ($block->subject): ?>
	  <h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
	<?php endif;?>
	  <?php print render($title_suffix); ?>

	  <div class="content"<?php print $content_attributes; ?>>
	    <?php print $content ?>
	  </div>
	</div>


<?php } ?>

